export const state = () => ({
  username: "",
  email: "",
  password: "",
  jwt: ""
})

export const mutations = {
  INSERT(state, data){
    state.username = data.username
    state.email = data.email
    state.password = data.password
    state.jwt = data.jwt
  },
}

export const actions = {
  async insert(context, data){
    
    const aux = await this.$axios.post("/register", data)
    .then(function (res) {
      return res.data
    })
    .catch(function (e){
      console.log(e.response.data)
      return(e.response.status)
    })
    
    if(aux.accessToken){
      data.jwt = aux.accessToken
      context.commit("INSERT", data)
      return 201
    } else{
      return aux
    }
  },

  async login(context, data){
    
    const aux = await this.$axios.post("/login", data)
    .then(function (res) {
      return res.data
    })
    .catch(function (e){
      return(e.response.status)
    })

    if(aux.accessToken){
      data.jwt = aux.accessToken
      context.commit("INSERT", data)
      return 200
    } else{
      return aux
    }
  }

}